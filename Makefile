MOVIE_DIRS=$(shell ls movies/ | sed 's/ /\\\\/g')
MOVIE_HTML=$(subst \\,\ ,$(addprefix movies/, $(addsuffix /index.html, $(MOVIE_DIRS))))
MOVIE_NFO=$(subst \\,\ ,$(addprefix movies/, $(addsuffix /movie.nfo, $(MOVIE_DIRS))))

TVSHOW_DIRS=$(shell ls tvshows/ | sed 's/ /\\\\/g')
TVSHOW_NFO=$(subst \\,\ ,$(addprefix tvshows/, $(addsuffix /tvshow.nfo, $(TVSHOW_DIRS))))
TVSHOW_HTML=$(subst \\,\ ,$(addprefix tvshows/, $(addsuffix /index.html, $(TVSHOW_DIRS))))

PAGEFIND=../pagefind_extended
SAXON_XSLT=java -cp saxon-he-12.3.jar net.sf.saxon.Transform

.PHONY: all text_search director_pages actor_pages movie_pages tvshow_pages

all: index.html tags.html movie_pages director_pages actor_pages tag_pages genre_pages text_search

text_search:
	$(PAGEFIND) --site .

index.html: movies.xml xslt/index.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:movies.xml -o:index.html -xsl:xslt/index.xsl 

tvshows.html: $(TVSHOW_NFO) xslt/tvshows.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -o:$@ -xsl:xslt/tvshows.xsl -it:main 

tags.html: tags.xml xslt/tags.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:tags.xml -xsl:xslt/tags.xsl -o:tags.html

movies.xml: $(MOVIE_NFO) movies.sh
	./movies.sh

%s.xml: xslt/xml-grouping.xsl movies.xml xslt/configuration.xsl
	$(SAXON_XSLT) -s:movies.xml -xsl:xslt/xml-grouping.xsl -o:$@ group=$(subst s.xml,,$@)

tags.xml: xslt/xml-grouping.xsl movies.xml xslt/configuration.xsl
	$(SAXON_XSLT) -s:movies.xml -xsl:xslt/xml-grouping.xsl -o:$@ group=tag min_size=3

director_pages: directors.xml xslt/person.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:directors.xml -xsl:xslt/person.xsl 

actor_pages: actors.xml xslt/person.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:actors.xml -xsl:xslt/person.xsl 

tag_pages: tags.xml xslt/person.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:tags.xml -xsl:xslt/person.xsl

genre_pages: genres.xml xslt/person.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -s:genres.xml -xsl:xslt/person.xsl

movie_pages: $(MOVIE_HTML)

movies/%/index.html: movies/%/movie.nfo tags.xml xslt/movie.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -o:"$@" -xsl:xslt/movie.xsl -s:"$<"

tvshow_pages: $(TVSHOW_HTML)

tvshows/%/index.html: tvshows/%/tvshow.nfo xslt/tvshow.xsl xslt/core.xsl xslt/configuration.xsl
	$(SAXON_XSLT) -xsl:xslt/tvshow.xsl dir="$@" -o:"$@" -s:"$<"

