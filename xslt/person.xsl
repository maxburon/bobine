<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <xsl:for-each select="*/*">
      <xsl:result-document href="{/*/name()}/{name}.html" method="html">
        <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
          <head>
            <xsl:call-template name="head">
              <xsl:with-param name="title">
                <xsl:value-of select="name"/>
                <xsl:value-of select="' - '"/>
                <xsl:value-of select="./name()"/>
              </xsl:with-param>
            </xsl:call-template>
          </head>
          <body>
            <xsl:call-template name="nav"/>
            <main data-pagefind-body="">
              <header>
                <xsl:apply-templates select="thumb"/>
                <xsl:apply-templates select="name"/>
                <xsl:apply-templates select="profile"/>
              </header>
              <xsl:apply-templates select="movies"/>
            </main>
          </body>
        </html>
      </xsl:result-document>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="name">
    <h1 class="center">
      <xsl:if test="parent::node()[name() = 'tag']">#</xsl:if>
      <xsl:value-of select="."/>
    </h1>
  </xsl:template>

  <xsl:template match="profile">
    <div class="item-array info-line"  data-pagefind-ignore="all">
      <p class="year item" data-pagefind-sort="year">
        <a>
          <xsl:attribute name="href" >
            <xsl:value-of select="."/>
          </xsl:attribute>
          <xsl:text>tmdb</xsl:text>
        </a>
      </p>
    </div> 
  </xsl:template>

  <xsl:template match="thumb">
    <img class="poster" loading="lazy" data-pagefind-meta="image[src], image_alt[alt]">
      <xsl:attribute name="src">
        <xsl:value-of select="."/>
      </xsl:attribute>
    </img>
  </xsl:template>
  
</xsl:stylesheet>

