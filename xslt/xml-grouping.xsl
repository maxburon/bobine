<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">
  <xsl:import href="core.xsl"/>
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:param name="group" select="'genre'"/>
  <xsl:param name="min_size" select="'1'"/>
  
  <xsl:template match="/">
    <xsl:element name="{$group}s">
      <xsl:choose>
        <xsl:when test="//movie/*[local-name() eq $group]/name">
          
          <xsl:for-each-group select="//movie" group-by="*[local-name() eq $group]/name">
            <xsl:if test="(count(current-group()) >= number($min_size)) and (current-group()/*[local-name() eq $group][position() lt 10][name eq current-grouping-key()])">
            <xsl:call-template name="group-xml">
              <xsl:with-param name="elts" select="current-group()"/>
              <xsl:with-param name="key" select="current-grouping-key()"/>
            </xsl:call-template>
          </xsl:if>
          </xsl:for-each-group>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each-group select="//movie" group-by="*[local-name() eq $group]">
            <xsl:if test="(count(current-group()) >= number($min_size)) and (current-group()/*[local-name() eq $group][position() lt 10][. eq current-grouping-key()])">
            <xsl:call-template name="group-xml">
              <xsl:with-param name="elts" select="current-group()"/>
              <xsl:with-param name="key" select="current-grouping-key()"/>
            </xsl:call-template>
            </xsl:if>
          </xsl:for-each-group>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:element>
  </xsl:template>

  <xsl:template name="group-xml">
    <xsl:param name="key"/>
    <xsl:param name="elts"/>

    <!-- <xsl:if test="count($elts) >= number($min_size)"> -->
      <xsl:element name="{$group}">
        <xsl:attribute name="count" select="count($elts)"/>
        <name>
          <xsl:value-of select="$key"/>
        </name>
        <xsl:if test="$group='actor'">
          <profile>
            <xsl:value-of select="($elts/actor[name=$key][profile/text()]/profile)[1]"/>
          </profile>
          <thumb>
            <xsl:value-of select="tmm:actor_thumb_url($elts[1], $elts[1]/actor[name eq $key])"/>
          </thumb>
        </xsl:if>
        <movies>
          <xsl:apply-templates select="$elts">
            <xsl:sort select="year" order="descending"/>
          </xsl:apply-templates>
        </movies>
      </xsl:element>
    <!-- </xsl:if> -->
  </xsl:template>

  <xsl:template match="movie">
    <movie>
      <title>
        <xsl:value-of select="title" />
      </title>
      <year>
        <xsl:value-of select="year" />
      </year>
    </movie>
  </xsl:template>
</xsl:stylesheet>

