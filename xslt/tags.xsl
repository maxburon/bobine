<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>


  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="'tags'"/>
        </xsl:call-template>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main>
          <h1 class="center">Tags</h1>
          <xsl:apply-templates/>
        </main>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="tags">
    <ul class="tags">
      <xsl:apply-templates>
        <xsl:sort select="name"/>
      </xsl:apply-templates>
    </ul>
  </xsl:template>

  <xsl:template match="tag">
    <li class="tag">
      <a href="{tmm:tag_url(name)}" style="font-size: {0.75 + (math:log(./@count)) div 3}em;"
         xmlns:math="http://www.w3.org/2005/xpath-functions/math"
         xmlns:op="http://www.w3.org/2005/xpath-functions/op">
        <xsl:value-of select="name"/>
      </a>
    </li>
  </xsl:template>
</xsl:stylesheet>

