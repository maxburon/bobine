<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:tmm="https://www.tinymediamanager.org"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

  <xsl:param name="dir"/>

  <xsl:variable name="tvshow_seasons">
    <xsl:value-of select="concat('../', tmm:dirname($dir),'?select=*.nfo;recurse=yes')"/>
  </xsl:variable>
  
  <xsl:template match="/tvshow">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="title"/>
        </xsl:call-template>
        <style>
          .movie-header .fanart {
                background-image: url('fanart.jpg');
              }
          @media only screen and (min-width: 900px) {
             .movie-header .fanart {
                 background-image: url('banner.jpg'), url('fanart.jpg');
              }
          }
        </style>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main data-pagefind-body=""> 
          <header class="movie-header">
            <div class="fanart">
            </div> 
            <div class="movie-header-bottom">
              <div class="poster">
                <img data-pagefind-meta="image[src], image_alt[alt]">
                  <xsl:attribute name="src">
                    <xsl:value-of select="tmm:tvshow_poster_url(.)"/>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    Poster of <xsl:value-of select="title"/>
                  </xsl:attribute>
                </img>
              </div> 
              <div class="info">
                <h1>
                  <xsl:value-of select="title"/>
                </h1> 
                <div class="item-array info-line" >
                  <xsl:apply-templates select="rating|year" />
                </div> 
                <div class="key-value genres info-line" data-pagefind-ignore="all">
                  <xsl:for-each select="genre">
                    <span class="info">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:call-template name="genre_url">
                            <xsl:with-param name="genre" select="."/>
                          </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                      </a>
                    </span>
                  </xsl:for-each>
                </div> 
                <div class="key-value director info-line">
                  <xsl:for-each select="director">
                    <span class="info">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:call-template name="director_url">
                            <xsl:with-param name="director_name" select="."/>
                          </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                      </a>
                    </span>
                  </xsl:for-each>
                </div>
              </div> 
            </div>
          </header> 
          <section>
            <p class="plot" data-pagefind-weight="2">
              <xsl:value-of select="plot"/>
            </p>
            <div class="cards">
              <xsl:for-each-group select="collection($tvshow_seasons)" group-by="//season">
                <xsl:sort select="current-grouping-key()" />
                <xsl:call-template name="season_poster">
                  <xsl:with-param name="ep" select="current-group()[1]/episodedetails"/>
                </xsl:call-template>
              </xsl:for-each-group>
            </div>
            <!-- <h2 class="center">Actors</h2> -->
            <!-- <div class="cards actors"> -->
            <!--   <xsl:apply-templates select="actor" /> -->
            <!-- </div> -->
          </section>
        </main>

      </body>
    </html>
  </xsl:template>

  <xsl:template name="season_poster">
    <xsl:param name="ep"/>
    <div class="card movie">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="tmm:tvshow_season_name($ep/season)" />
        </xsl:attribute>
        <img class="poster" loading="lazy">
          <xsl:attribute name="src">
            <xsl:value-of select="tmm:tvshow_season_poster($ep/season)" />
          </xsl:attribute>
        </img>
        <p><xsl:value-of select="tmm:tvshow_season_name($ep/season)"/></p>
      </a>
    </div>
  </xsl:template>

    <xsl:function name="tmm:dirname" as="xs:string">
    <xsl:param name="pfile" as="xs:string"/>
    <xsl:sequence select=
     "tmm:reverseStr(substring-after(tmm:reverseStr($pfile), '/'))
     " />
  </xsl:function>

  <xsl:function name="tmm:reverseStr" as="xs:string">
    <xsl:param name="pStr" as="xs:string"/>

    <xsl:sequence select=
    "codepoints-to-string(reverse(string-to-codepoints($pStr)))"/>
  </xsl:function>
  
</xsl:stylesheet>

