<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">

  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
  <xsl:param name="dir"/>

  <xsl:variable name="tvshow_ep_nfo">
    <xsl:value-of select="concat('../', $dir,'?select=*.nfo')"/>
  </xsl:variable>

  <xsl:variable name="tvshow_doc_path">
    <xsl:value-of select="concat('../', $dir,'/../tvshow.nfo')"/>
  </xsl:variable>

  <xsl:variable name="tvshow_doc" select="doc(concat('../', $dir,'/../tvshow.nfo'))/tvshow"/>

  <xsl:template name="main">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="'Season'"/>
        </xsl:call-template>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main data-pagefind-body=""> 
          <xsl:call-template name="header"/>
          <xsl:for-each select="collection($tvshow_ep_nfo)">
            <xsl:sort select="number(//episode)"/>
            <xsl:apply-templates/>
          </xsl:for-each>
        </main>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="header">
    <xsl:variable name="episode" select="collection($tvshow_ep_nfo)[1]"/>
    <header class="movie-header">
      <xsl:value-of select="$tvshow_doc/tvshow"/>
      <!-- <xsl:value-of select="doc($tvshow_doc_path)/tvshow"/> -->
      <div class="fanart" style="background-image: url('{tmm:tvshow_season_thumb_url($tvshow_doc, $episode//season)}')"> </div> 
      <div class="movie-header-bottom">
          <img class="poster" loading="lazy" data-pagefind-meta="image[src], image_alt[alt]">
            <xsl:attribute name="src">
              <xsl:value-of select="tmm:tvshow_season_poster_url($tvshow_doc,$episode//season)"/>
            </xsl:attribute>
          </img>
        <div class="info">
          <h1 class="center">
            <a href=".."><xsl:value-of select="$episode//showtitle"/></a><br/>
            <xsl:text>Season </xsl:text><xsl:value-of select="$episode//season"/>
          </h1>
        </div>
        <h2 class="center">
          <xsl:value-of select="count(collection($tvshow_ep_nfo))"/> Episodes
        </h2>
      </div>
    </header>
  </xsl:template>

</xsl:stylesheet>
