<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">

  <xsl:variable name="url_separator" >/</xsl:variable>
  
  <!-- WEBSITE -->
  <xsl:variable name="website_name" >Bobine</xsl:variable>
  <xsl:variable name="website_host" ></xsl:variable>
  <xsl:variable name="website_base" >/</xsl:variable>
  <xsl:variable name="website_url" >
    <xsl:value-of select="$website_host"/>
    <xsl:value-of select="$website_base"/>
  </xsl:variable>

  <!-- DIRECTORIES   -->
  <xsl:variable name="actors_dir" >actors</xsl:variable>
  <xsl:variable name="directors_dir" >directors</xsl:variable>
  <xsl:variable name="genres_dir" >genres</xsl:variable>
  <xsl:variable name="movies_dir" >movies</xsl:variable>
  <xsl:variable name="tags_dir" >tags</xsl:variable>
  <xsl:variable name="tvshows_dir" >tvshows</xsl:variable>


  <!-- MOVIE FILES -->

  <xsl:function name="tmm:movie_name">
    <xsl:param name="movie"/>
    <xsl:variable name ="movie_title" select="replace(replace(replace($movie/title, '\s?:', ' -'), '\s?\?', ''), '/', ' ')"/>
    <xsl:choose>
      <xsl:when test="$movie/year/text()">
        <xsl:value-of select="concat($movie_title, ' (', $movie/year, ')')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$movie_title"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="tmm:path_extension">
    <xsl:param name="path"/>
    <xsl:value-of select="tokenize($path, '\.')[last()]"/>
  </xsl:function>

  <xsl:template name="movie_file">
    <xsl:param name="movie"/>
    <xsl:value-of select="concat(tmm:movie_name($movie), '.', tmm:path_extension($movie/original_filename))"/>
  </xsl:template>

  <xsl:template name="movie_subtitle">
    <xsl:param name="movie"/>
    <xsl:param name="lang"/>
    <xsl:value-of select="concat(tmm:movie_name($movie), '.', $lang, '.srt')"/>
  </xsl:template>

  <xsl:template name="movie_poster">
    <xsl:param name="movie"/>
    <xsl:value-of select="'poster.jpg'"/>
  </xsl:template>

  <!-- TVSHOW FILES -->

  <xsl:function name="tmm:tvshow_name_subs">
    <xsl:param name="showtitle"/>
    <xsl:value-of select="replace(replace($showtitle, '(\s?:|\s?\?)', ''), '/', ' ')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_name">
    <!-- tvshow or episode xml -->
    <xsl:param name="tvshow"/>
    <xsl:choose>
      <xsl:when test="$tvshow/title">
        <xsl:value-of select="tmm:tvshow_name_subs($tvshow/title)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="tmm:tvshow_name_subs($tvshow/showtitle)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="tmm:tvshow_dir">
    <!-- tvshow or episode xml -->
    <xsl:param name="tvshow"/>
    <xsl:value-of select="concat(tmm:tvshow_name($tvshow), ' (', $tvshow/year, ')')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_poster">
    <xsl:param name="tvshow"/>
    <xsl:value-of select="'poster.jpg'"/>
  </xsl:function>

 <xsl:function name="tmm:tvshow_season_name">
    <xsl:param name="season"/>
    <xsl:value-of select="concat('Season ', $season)"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_season_poster">
    <xsl:param name="season"/>
    <xsl:value-of select="concat('season',tmm:tvshow_number($season),'-poster.jpg')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_season_thumb">
    <xsl:param name="season"/>
    <xsl:value-of select="concat('season',tmm:tvshow_number($season),'-thumb.jpg')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_number">
    <xsl:param name="number"/>
    <xsl:choose>
      <xsl:when test="number($number) lt 10">
        <xsl:value-of select="concat('0',$number)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$number"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="tmm:tvshow_ep_name">
    <xsl:param name="ep"/>
    <xsl:value-of select="concat(tmm:tvshow_name_subs($ep/showtitle), ' - S', tmm:tvshow_number($ep/season), 'E', tmm:tvshow_number($ep/episode), ' - ', replace($ep/title, '(\s?:|\s?\?)', ''))"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_ep_subtitle">
    <xsl:param name="ep"/>
    <xsl:param name="lang"/>
    <xsl:value-of select="concat(tmm:tvshow_ep_name($ep), '.', $lang, '.srt')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_ep_file">
    <xsl:param name="ep"/>
    <xsl:value-of select="concat(tmm:tvshow_ep_name($ep), '.', substring-after($ep/original_filename, '.'))" />
  </xsl:function>

  <xsl:function name="tmm:tvshow_ep_thumb">
    <xsl:param name="ep"/>
    <xsl:value-of select="concat(tmm:tvshow_ep_name($ep), '-thumb.jpg')"/>
  </xsl:function>

</xsl:stylesheet>
