<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:tmm="https://www.tinymediamanager.org"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

  <xsl:variable name="tvshow_dirs">
    <xsl:value-of select="'../tvshows/?select=tvshow.nfo;recurse=yes'"/>
  </xsl:variable>
  
  <xsl:template name="main">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="'tvshows'"/>
        </xsl:call-template>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main>
          <div id="movies">
            <h1 class="center">TV shows</h1>
            <div class="cards">
              <xsl:for-each select="collection($tvshow_dirs)">
                <xsl:sort select="tvshow/title" order="ascending"/>
                <xsl:apply-templates select="tvshow"/>
              </xsl:for-each>
            </div>
          </div>
        </main>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="tvshow">
    <div class="card movie">
      <a href="{tmm:tvshow_url(.)}">
        <img src="{tmm:tvshow_poster_url(.)}" class="poster" loading="lazy" />
        <p><xsl:value-of select="title"/></p>
      </a>
      <p><xsl:value-of select="year"/></p>
    </div>
  </xsl:template>

</xsl:stylesheet>

