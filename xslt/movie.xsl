<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

  <xsl:variable name="tags_doc" select="doc(concat('../','tags.xml'))/tags"/>
  
  <xsl:template match="/movie">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="title"/>
        </xsl:call-template>
        <style>
          .movie-header .fanart {
          background-image: url('fanart.jpg');
          }
          @media only screen and (min-width: 900px) {
          .movie-header .fanart {
          background-image: url('banner.jpg'), url('fanart.jpg');
          }
          }
        </style>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main data-pagefind-body=""> 
          <header class="movie-header">
            <div class="fanart">
            </div> 
            <div class="movie-header-bottom">
              <div class="poster">
                <img data-pagefind-meta="image[src], image_alt[alt]">
                  <xsl:attribute name="src">
                    <xsl:call-template name="movie_poster_url">
                      <xsl:with-param name="movie" select="."/>
                    </xsl:call-template>
                  </xsl:attribute>
                  <xsl:attribute name="alt">
                    Poster of <xsl:value-of select="title"/>
                  </xsl:attribute>
                </img>
              </div> 
              <div class="info">
                <h1>
                  <xsl:value-of select="title"/>
                </h1> 
                <div class="item-array info-line" >
                  <xsl:apply-templates select="ratings|year|runtime" />
                </div>
                <div class="key-value director info-line">
                  <xsl:for-each select="director">
                    <span class="info">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:call-template name="director_url">
                            <xsl:with-param name="director_name" select="."/>
                          </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                      </a>
                    </span>
                  </xsl:for-each>
                </div>
                <div class="key-value genres info-line" data-pagefind-ignore="all">
                  <xsl:for-each select="genre">
                    <span class="info">
                      <a>
                        <xsl:attribute name="href">
                          <xsl:call-template name="genre_url">
                            <xsl:with-param name="genre" select="."/>
                          </xsl:call-template>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                      </a>
                    </span>
                  </xsl:for-each>
                </div> 
              </div>
              <div class="key-value tags info-line" data-pagefind-ignore="all">
                <xsl:for-each select="tag">
                  <xsl:variable name="tag" select="."/>
                  <xsl:if test="$tags_doc/tag[name eq $tag]">
                    <span class="info tag">
                      <a href="{tmm:tag_url($tag)}">
                        <xsl:value-of select="$tag"/>
                      </a>
                    </span>
                  </xsl:if>
                </xsl:for-each>
              </div> 
            </div>
          </header> 
          <section>
            <div class="item-array" data-pagefind-ignore="all">
              <label class="download item icon-box" for="vlc-instruction-box">
                <div class="icon">
                  <xsl:call-template name="icon_play"/>
                </div>
                <div class="description">
                  Play in VLC
                </div>
                <input type="checkbox" id="vlc-instruction-box" class="instruction-box" checked="" value="0"/>
                <div class="instruction">
                  <div class="instruction-content">
                    <label for="vlc-instruction-box" class="instruction-close">
                      close
                    </label>
                    <h4>How to:</h4>
                    <ul>
                      <li>Copy the download link</li>
                      <li>Open VLC</li>
                      <li>Media>Open Network Stream (Ctrl+N)</li>
                      <li>Paste the link and play</li>
                    </ul>
                  </div>
                </div>
              </label>
              <div class="download item icon-box">
                <a download="">
                  <xsl:attribute name="href">
                    <xsl:call-template name="movie_file_url">
                      <xsl:with-param name="movie" select="."/>
                    </xsl:call-template>
                  </xsl:attribute>
                  <div class="icon">
                    <xsl:call-template name="icon_download"/>
                  </div> 
                  <div class="description">Download</div>
                </a>
              </div>
              <xsl:for-each select="//subtitle/language">
                <div class="subtitle item icon-box">
                  <a>
                    <xsl:attribute name="href">
                      <xsl:call-template name="movie_subtitle_url">
                        <xsl:with-param name="movie" select="/movie"/>
                        <xsl:with-param name="lang" select="."/>
                      </xsl:call-template>
                    </xsl:attribute>
                    <div class="icon">
                      <xsl:call-template name="icon_subtitle"/>
                    </div> 
                    <div class="description">
                      Subtitle (<xsl:value-of select="."/>)
                    </div>
                  </a> 
                </div>
              </xsl:for-each>
            </div> 
            <p class="plot" data-pagefind-weight="2">
              <xsl:value-of select="plot"/>
            </p>
            <div class="cards actors">
              <xsl:apply-templates select="actor" />
            </div>
          </section>
        </main>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>

