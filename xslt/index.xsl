<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="core.xsl"/>
  <xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
      <head>
        <xsl:call-template name="head">
          <xsl:with-param name="title" select="'movies'"/>
        </xsl:call-template>
      </head>
      <body>
        <xsl:call-template name="nav"/>
        <main>
          <div id="latest">
            <h1 class="center">Latest movies
            <a href="/feed" class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
                <title>Logo Rss
                </title>
                <path d="M108.56 342.78a60.34 60.34 0 1060.56 60.44 60.63 60.63 0 00-60.56-60.44z">
                </path>
                <path d="M48 186.67v86.55c52 0 101.94 15.39 138.67 52.11s52 86.56 52 138.67h86.66c0-151.56-125.66-277.33-277.33-277.33z">
                </path>
                <path d="M48 48v86.56c185.25 0 329.22 144.08 329.22 329.44H464C464 234.66 277.67 48 48 48z">
                </path>
              </svg>
            </a>
            </h1>
            <div class="cards">
              <xsl:for-each select="//movie">
                <xsl:sort select="dateadded" order="descending"/>
                <xsl:if test="position() lt 20">
                  <xsl:apply-templates select="."/>
                </xsl:if>
              </xsl:for-each>
            </div>
          </div>
          <div>
            <h1 class="center">Movies</h1>
            <div class="key-value info-line center">
              <xsl:for-each select="//movie/genre[not(.=following::genre)]">
                <xsl:sort select="."/>
                <span class="info">
                  <a href="./genres/{.}.html"><xsl:value-of select="."/> </a>
                </span>
              </xsl:for-each>
            </div>
          </div>
          <div id="movies">
            <h1 class="center">Movies by year</h1>
              <xsl:for-each-group select="document('../movies.xml')/movies/movie[year/text()]"
                                  group-by="floor(year div 10)">
                <xsl:sort select="current-grouping-key()" order="descending"/>
                <xsl:variable name="count_per_year" select="count(current-group())"/>

                <details>
                  <xsl:if test="position() = 1">
                    <xsl:attribute name="open"></xsl:attribute>
                  </xsl:if>
                  <summary class="center">
                    <h2><xsl:value-of select="current-grouping-key() * 10"/>s</h2>
                  </summary>
                  <xsl:for-each-group select="current-group()"
                                      group-by="year">
                    <xsl:sort select="current-grouping-key()" order="descending"/>
                    <details>
                      <xsl:if test="$count_per_year lt 150">
                        <xsl:attribute name="open" select="''"/>
                      </xsl:if>
                      <summary class="center">
                        <h3><xsl:value-of select="current-grouping-key()"/></h3>
                      </summary>
                      <div class="cards">
                        <xsl:apply-templates select="current-group()">
                          <xsl:sort select="premiered" order="descending"/>
                        </xsl:apply-templates>
                      </div>
                    </details>
                  </xsl:for-each-group>
                </details>
              </xsl:for-each-group>
            </div>
        </main>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>

