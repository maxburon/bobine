<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tmm="https://www.tinymediamanager.org">

  <xsl:import href="configuration.xsl"/>

  <!-- HTML -->
  
  <xsl:template name="head">
    <xsl:param name="title"/>
    <head>
      <title>
        <xsl:value-of select="$title"/> - <xsl:value-of select="$website_name"/>
      </title>
      <meta charset="utf-8"/>
      <meta content="width=device-width,initial-scale=1" name="viewport"/> 
      <meta content="#333333" name="theme-color"/>
      <link href="/favicon.icon" rel="icon" type="image/x-icon"/> 
      <link href="/feed" rel="alternate" type="application/rss+xml" title="rss"/>
      <link href="/pagefind/pagefind-ui.css" rel="stylesheet"/>
      <link href="/global.css" rel="stylesheet"/> 
      <script src="/pagefind/pagefind-ui.js"></script>
      <script>
        window.addEventListener('DOMContentLoaded', (event) => {
        new PagefindUI({ element: "#search", showSubResults: true });
        });
      </script>
    </head>
  </xsl:template>

  <xsl:template name="nav">
    <nav>
      <ul>
        <li>
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$website_base"/>
            </xsl:attribute>
            movies
          </a>
        </li> 
        <li>
          <a href="/tvshows.html">tv shows</a>
        </li> 
        <li>
          <a href="/about">about</a>
        </li>
      </ul>
      <div id="search"></div>
    </nav> 
  </xsl:template>

  <!-- URL -->

  <xsl:function name="tmm:movie_url">
    <xsl:param name="movie"/>
    <xsl:value-of select="concat($website_url, $movies_dir, $url_separator, tmm:movie_name($movie))"/>
  </xsl:function>

  <xsl:template name="movie_poster_url">
    <xsl:param name="movie"/>
    <xsl:value-of select="tmm:movie_url($movie)"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:call-template name="movie_poster">
      <xsl:with-param name="movie" select="$movie"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="movie_file_url">
    <xsl:param name="movie"/>
    <xsl:value-of select="tmm:movie_url($movie)"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:call-template name="movie_file">
      <xsl:with-param name="movie" select="$movie"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="movie_subtitle_url">
    <xsl:param name="movie"/>
    <xsl:param name="lang"/>
    <xsl:value-of select="tmm:movie_url($movie)"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:call-template name="movie_subtitle">
      <xsl:with-param name="movie" select="$movie"/>
      <xsl:with-param name="lang" select="$lang"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="actor_url">
    <xsl:param name="actor_name"/>
    <xsl:value-of select="$website_url"/>
    <xsl:value-of select="$actors_dir"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:value-of select="$actor_name"/>
    <xsl:value-of select="'.html'"/>
  </xsl:template>

  <xsl:function name="tmm:actor_thumb_url">
    <xsl:param name="movie"/>
    <xsl:param name="actor"/>
    <xsl:choose>
      <xsl:when test="$actor/thumb[text()]">
        <xsl:value-of select="concat(tmm:movie_url($movie), $url_separator, '.actors', $url_separator, translate($actor/name,' ', '_'), '.jpg')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'/default_person.png'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:template name="director_url">
    <xsl:param name="director_name"/>
    <xsl:value-of select="$website_url"/>
    <xsl:value-of select="$directors_dir"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:value-of select="$director_name"/>
    <xsl:value-of select="'.html'"/>
  </xsl:template>

  <xsl:template name="genre_url">
    <xsl:param name="genre"/>
    <xsl:value-of select="$website_url"/>
    <xsl:value-of select="$genres_dir"/>
    <xsl:value-of select="$url_separator"/>
    <xsl:value-of select="$genre"/>
    <xsl:value-of select="'.html'"/>
  </xsl:template>

  <xsl:function name="tmm:tag_url">
    <xsl:param name="tag"/>
    <xsl:value-of select="concat($website_url, $tags_dir, $url_separator, $tag, '.html')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_dir_url">
    <xsl:param name="tvshow"/>
    <xsl:value-of select="concat($website_url, $tvshows_dir, $url_separator, tmm:tvshow_dir($tvshow))"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_url">
    <xsl:param name="tvshow"/>
    <xsl:value-of select="concat(tmm:tvshow_dir_url($tvshow), $url_separator, 'index.html')"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_season_poster_url">
    <xsl:param name="tvshow"/>
    <xsl:param name="season"/>
    <xsl:value-of select="concat(tmm:tvshow_dir_url($tvshow), $url_separator, tmm:tvshow_season_poster($season))"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_season_thumb_url">
    <xsl:param name="tvshow"/>
    <xsl:param name="season"/>
    <xsl:value-of select="concat(tmm:tvshow_dir_url($tvshow), $url_separator, tmm:tvshow_season_thumb($season))"/>
  </xsl:function>

  <xsl:function name="tmm:tvshow_poster_url">
    <xsl:param name="tvshow"/>
    <xsl:value-of select="concat(tmm:tvshow_dir_url($tvshow), $url_separator, tmm:tvshow_poster($tvshow))"/>
  </xsl:function>

  <!-- ICONS -->

  <xsl:template name="icon_play">
    <svg xmlns="http://www.w3.org/2000/svg" class="ionicon" viewBox="0 0 512 512">
      <path d="M133 440a35.37 35.37 0 01-17.5-4.67c-12-6.8-19.46-20-19.46-34.33V111c0-14.37 7.46-27.53 19.46-34.33a35.13 35.13 0 0135.77.45l247.85 148.36a36 36 0 010 61l-247.89 148.4A35.5 35.5 0 01133 440z">
      </path>
    </svg>
  </xsl:template>

  <xsl:template name="icon_subtitle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="1em">
      <path d="M32 96v320h448V96H32zm406 159.8c0 23.4-1.4 41.2-3.3 70.2s-16.8 49.4-51.7 52.6c-34.9 3.2-83.8 3.5-127 3.4-42.9.1-92-.1-127-3.4-34.9-3.2-49.7-23.6-51.7-52.6S74 279.2 74 255.8c0-23.4.1-38.6 3.3-70.2s20.1-49.2 51.7-52.4 86-3.2 127-3.2 95.4 0 127 3.2c31.6 3.2 48.5 20.9 51.7 52.4 3.2 31.6 3.3 46.9 3.3 70.2z">
      </path>
      <path d="M357.5 280.4v.7c0 16.3-10.1 25.9-23.6 25.9-13.5 0-22.6-10.8-23.9-25.9 0 0-1.2-7.9-1.2-23.9s1.4-26 1.4-26c2.4-17 10.7-25.9 24.2-25.9 13.4 0 24.1 11.6 24.1 29.2v.5h45.1c0-21.9-5.5-41.6-16.6-54-11-12.4-27.5-18.6-49.3-18.6-10.9 0-20.9 1.4-30 4.3-9.1 2.9-17 7.9-23.6 15.1-6.6 7.2-11.7 16.8-15.4 28.9-3.6 12.1-5.5 27.3-5.5 45.7 0 18 1.5 33 4.4 45.1 3 12.1 7.3 21.7 13.1 28.9 5.8 7.2 13.1 12.2 21.8 15 8.8 2.8 19.1 4.2 30.9 4.2 25 0 43-6.4 53.8-18.7 10.8-12.3 16.2-30.3 16.2-53.9h-46.1c.2 0 .2 2.5.2 3.4zM202.6 280.4v.7c0 16.3-10.1 25.9-23.6 25.9-13.5 0-22.6-10.8-23.9-25.9 0 0-1.2-7.9-1.2-23.9s1.4-26 1.4-26c2.4-17 10.7-25.9 24.2-25.9 13.4 0 24.1 11.6 24.1 29.2v.5h45.1c0-21.9-5.5-41.6-16.6-54-11-12.4-27.5-18.6-49.3-18.6-10.9 0-20.9 1.4-30 4.3-9.1 2.9-17 7.9-23.6 15.1-6.6 7.2-11.7 16.8-15.4 28.9-3.6 12.1-5.5 27.3-5.5 45.7 0 18 1.5 33 4.4 45.1 3 12.1 7.3 21.7 13.1 28.9 5.8 7.2 13.1 12.2 21.8 15 8.8 2.8 19.1 4.2 30.9 4.2 25 0 43-6.4 53.8-18.7 10.8-12.3 16.2-30.3 16.2-53.9h-46.1c.2 0 .2 2.5.2 3.4z">
      </path>
    </svg>
  </xsl:template>

  <xsl:template name="icon_download">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
      <path d="M416 199.5h-91.4V64H187.4v135.5H96l160 158.1 160-158.1zM96 402.8V448h320v-45.2H96z">
      </path>
    </svg>
  </xsl:template>

  <!-- TEMPLATES -->

  <xsl:template match="movie">
    <div class="card movie">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="tmm:movie_url(.)"/>
        </xsl:attribute>
        <img class="poster" loading="lazy">
          <xsl:attribute name="src">
            <xsl:call-template name="movie_poster_url">
              <xsl:with-param name="movie" select="."/>
            </xsl:call-template>
          </xsl:attribute>
        </img>
        <p><xsl:value-of select="title"/></p>
      </a>
      <p><xsl:value-of select="year"/></p>
    </div>
  </xsl:template>

  <xsl:template match="movies">
    <div class="cards" data-pagefind-ignore="all">
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="rating">
    <p class="rating item" data-pagefind-ignore="all">
      ⭐ <xsl:choose>
      <xsl:when test="value"><xsl:value-of select="value"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
    </xsl:choose>
    </p>
  </xsl:template>

  <xsl:template match="year">
    <p class="year item" data-pagefind-sort="year">
      <xsl:value-of select="."/>
    </p>
  </xsl:template>

  <xsl:template match="runtime">
    <p class="runtime item" data-pagefind-ignore="all">
      <xsl:value-of select="."/>
    </p>
  </xsl:template>

  <xsl:template match="actor">
    <div class="actor card">
      <a>
        <xsl:attribute name="href">
          <xsl:call-template name="actor_url">
            <xsl:with-param name="actor_name" select="name"/>
          </xsl:call-template>
        </xsl:attribute>
        <xsl:call-template name="actor_img">
          <xsl:with-param name="movie" select="/movie"/>
          <xsl:with-param name="actor" select="."/>
        </xsl:call-template>
        <p><xsl:value-of select="role"/></p>
        <p><xsl:value-of select="name"/></p>
      </a>
    </div>
  </xsl:template>

  <xsl:template name="actor_img">
    <xsl:param name="movie"/>
    <xsl:param name="actor"/>
    <img class="poster" loading="lazy">
      <xsl:attribute name="src">
        <xsl:value-of select="tmm:actor_thumb_url($movie, $actor)"/>
      </xsl:attribute>
      <xsl:attribute name="alt"><xsl:value-of select="$actor/name"/>.jpg</xsl:attribute>
    </img>
  </xsl:template>

  
  <xsl:template match="episodedetails">
    <div class="episode">
      <img class="thumb" loading="lazy" alt="">
        <xsl:attribute name="src">
          <xsl:value-of select="tmm:tvshow_ep_thumb(.)" />
        </xsl:attribute>
      </img>
      <div class="info">
        <h3><xsl:value-of select="episode"/><xsl:text> </xsl:text><xsl:value-of select="title"/></h3>
        <div class="item-array info-line">
          <xsl:apply-templates select="(//rating[1])|runtime"/>
        </div>
        <div class="item-array" data-pagefind-ignore="all">
          <div class="download item icon-box">
            <div class="icon" title="help: open the m3u file with VLC">
              <xsl:call-template name="icon_play"/>
            </div>
            <div class="description">
              Play in VLC
            </div>
          </div>
          <div class="download item icon-box">
            <a download="">
              <xsl:attribute name="href">
                <xsl:value-of select="tmm:tvshow_ep_file(.)" />
              </xsl:attribute>
              <div class="icon">
                <xsl:call-template name="icon_download"/>
              </div> 
              <div class="description">Download</div>
            </a>
          </div>
          <xsl:for-each select="//subtitle/language">
            <div class="subtitle item icon-box">
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="tmm:tvshow_ep_subtitle(/episodedetails, .)" />
                </xsl:attribute>
                <div class="icon">
                  <xsl:call-template name="icon_subtitle"/>
                </div> 
                <div class="description">
                  Subtitle (<xsl:value-of select="."/>)
                </div>
              </a> 
            </div>
          </xsl:for-each>
        </div> 
        <p class="plot">
          <xsl:value-of select="plot"/>
        </p>
      </div>
    </div>
  </xsl:template>
</xsl:stylesheet>
