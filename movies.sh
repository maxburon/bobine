#!/bin/bash

MOVIES_NFO="movies/*/movie.nfo"
MOVIES_XML="movies.xml"

echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" > $MOVIES_XML
echo "<movies>" >> $MOVIES_XML
sed -i -e "s/\r//g" $MOVIES_NFO
tail -q -n +3 $MOVIES_NFO >>$MOVIES_XML
echo "</movies>" >> $MOVIES_XML
