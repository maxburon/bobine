#!/bin/bash

season_dirs=`ls -d tvshows/*/*/`

for dir in tvshows/*/*/;
do
    java -cp saxon-he-12.3.jar net.sf.saxon.Transform -xsl:xslt/season.xsl -it:main "dir=$dir" -o:"${dir}index.html"
    # cmd="java -cp saxon-he-12.3.jar net.sf.saxon.Transform -xsl:xslt/season.xsl -it:main \"dir=$dir\" " # -o:\"${dir}index.html\"" dir=\"$dir\" 
    # echo $cmd
    # ${cmd}
done
